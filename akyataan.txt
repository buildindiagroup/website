AKYA TAAN (Symphony of Harmony) is a festival of peace, love and harmony.

<strong>PLACE:</strong>
In all houses of Delhi (from balcony/courtyards/gardens, terrace)

<strong>PARTICIPANTS:</strong>
People of Delhi

<strong>TIME & DATE:</strong>
9 July, 2017, 10 AM

<strong>PURPOSE:</strong>
To demonstrate in unison that their homes do not provide a refuge to any terrorist or violent element.

<strong>MODALITY:</strong>
The festival involves a symphony by use of musical instruments, i.e. conchs, bells, drums, trumpets, etc, by people in unison. In housing societies, people can congregate and just clap together for two minutes following some kind of an address on nationalism as the theme. To provide a charged ambiance, people may go through banners displaying innovative ideas, make human chains, or have musical events on nationalism as the theme in their respective areas in the run up to the moment of celebration i.e., 10 AM.

<strong>DURATION:</strong>
Just two minutes in unison starting at 10 AM sharp

<strong>SIGNIFICANCE:</strong>
This will be the first major significant movement in India involving demonstration of our fundamental duties. Through this festival, a mass contempt would be unleashed against violence of any form.

<strong>COST INVOLVED:</strong>
No cost

<strong>WHAT NEXT:</strong>
It is a pilot programme in Delhi, a mini-India, wherepeople are drawn from various parts of the country. It has a potpourri culture. People of Delhi do understand better the threat of terrorism. The capital city is a favourite target for terrorists and anti-national elements because they get undue media attention. Anti-national and terrorist elements are very few in number. They need to be quarantined. Delhi has to be sanitised.  All that the people need to do is to join this symphony at the fixed time from their respective homes by creating a symphony with use of any instrument. We wish this programme should be repeated in the whole country on the occasion of Constitution Day on 26 November, 2017. If that happens, it could be a world model action point against terrorism.

<strong>A NOTE OF CAUTION:</strong>
People are advised not to hit the roads with this kind of an idea as it is likely to cause noise pollution and traffic problems. This is, in fact, a citizenry consciousness activity as preserving peace, unity and integrity is a duty cast upon us by our Constitution.

<a href="./akyataan-2017-invite">SEE THE 2017 INVITE</a>
