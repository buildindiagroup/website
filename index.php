<?php
include_once '../../bootstrap.php';
$local = $_SERVER['HTTP_HOST'] ==='localhost';
bootstrap(array_merge(include_once('../../_config.php') , array(
	'name' => 'BIG',
	'byline' => 'Cultivating GOOD Citizenry',
	'logo' => true,
	'safeName' => 'buildindiagroup',
	'url' => $local ? 'http://localhost/cs/doms/cselian-cms/ngos/buildindiagroup/' : 'https://cms.cselian.com/ngos/buildindiagroup/',
	'path' => __DIR__,
	'rich_footer' => [
		'title' => 'Build India Group',
		'description' => '(A Society Registered under Societies Registration Act 1860)<br/> Registration No. S/62309 Of 2008',
		'links' => [
			//TODO: remove target blank as sites incorporate our footer
			'About' => './about-us',
			'Team BIG' => './management',
			'Support Developer' => 'https://www.linkedin.com/in/ianamazi/" target="_blank',
		],
		'video' => 'LAD_-rupap0',
		//'right_widget' => '',
		'address' => 'D -289 Lower Ground Floor,<br />Defence Colony,<br /> New Delhi -110024',
		'phone' => '+919841223313', 'whatsapp' => '+919841223313',
		'email' => 'buildindia.country@gmail.com',
		'email_subject' => '?subject=Build+India+Group+Enquiry+from+web',
		'start_year' => '2008',
	],
)));
render();
?>
